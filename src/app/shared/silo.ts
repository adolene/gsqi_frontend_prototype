export class Silo {
	grain: string;
	gsqi: number;
	location: string;
	subLocation: string;
}