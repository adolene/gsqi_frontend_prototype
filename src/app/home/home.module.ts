import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialDesignModule } from '../../material/material-design.module';
//import { SiloSiteGridModule } from '../silo-site-grid/silo-site-grid.module';
import 'hammerjs';

import { HomeComponent } from './home.component';
import { SiloSiteGridComponent } from '../silo-site-grid/silo-site-grid.component';

@NgModule({
  declarations: [
    HomeComponent,
    SiloSiteGridComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialDesignModule,
    //SiloSiteGridModule
  ],
  providers: [],
  bootstrap: [ HomeComponent ]
})
export class HomeModule { }