import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  title = 'home';
  sliderVal: number = 0;
  selectedLocation = 0;
  selectedGrainType = 0;

  siloSites: string[] = [
    "Silo Site A",
    "Silo Site B",
    "Silo Site C",
    "Silo Site D",
    "Silo Site E"
  ];

  locations = [
    { value: 0, display: 'All'},
    { value: 1, display: 'Australia'},
    { value: 2, display: 'USA'}
  ];

  grainTypes = [
    { value: 0, display: 'All' },
    { value: 1, display: 'Barley' },
    { value: 2, display: 'Canola' }
  ];

  sliderChange(event: any) {
    this.sliderVal = event.value;
  }

}