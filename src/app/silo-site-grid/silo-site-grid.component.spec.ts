import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiloSiteGridComponent } from './silo-site-grid.component';

describe('SiloSiteGridComponent', () => {
  let component: SiloSiteGridComponent;
  let fixture: ComponentFixture<SiloSiteGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiloSiteGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiloSiteGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
