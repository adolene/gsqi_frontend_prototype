import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialDesignModule } from '../../material/material-design.module';
import 'hammerjs';

import { SiloSiteGridComponent } from './silo-site-grid.component';

@NgModule({
	declarations: [
		SiloSiteGridComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		MaterialDesignModule
	],
	providers: [],
	bootstrap: [ SiloSiteGridComponent ]
})
export class SiloSiteGridModule { }